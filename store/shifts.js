export const state = () => ({
  allShifts: [],
  currentShift: {
    title: '',
    description: '',
    formData: [],
    dates: [],
    id: Date.now(),
  },
});

export const mutations = {
  addShift(state, payload) {
    state.currentShift.formData = payload;
    state.allShifts.push(state.currentShift);
  },
  editShift(state, payload) {
    state.currentShift.formData = payload;
  },
  createShift(state, payload) {
    state.currentShift.formData = payload;
    state.allShifts.push(state.currentShift);
  },
  editTitle(state, val) {
    state.currentShift.title = val;
  },
  editDescription(state, val) {
    state.currentShift.description = val;
  },
  editDates(state, val) {
    state.currentShift.dates = val;
  },
  deleteDate(state, i) {
    state.currentShift.dates.splice(i, 1);
    state.currentShift.formData.splice(i, 1);
  },
  addFormData(state, payload) {
    state.currentShift.formData.push(payload);
  },
  getCurrentShift(state, id) {
    state.currentShift = state.allShifts.find((obj) => obj.id === id);
  },
  deleteShift(state) {
    const shiftIndex = state.allShifts.findIndex(
      (el) => el.id === state.currentShift.id
    );
    if (shiftIndex > -1) {
      state.allShifts.splice(shiftIndex, 1);
    }
  },
  clearShift(state) {
    state.currentShift = {
      title: '',
      description: '',
      formData: [],
      dates: [],
      id: Date.now(),
    };
  },
  editStartTime(state, { val, i }) {
    state.currentShift.formData[i].starttime = val;
  },
  editEndTime(state, { val, i }) {
    state.currentShift.formData[i].endtime = val;
  },
  editPrice(state, { val, i }) {
    state.currentShift.formData[i].price = val;
  },
  editSelected(state, { val, i }) {
    state.currentShift.formData[i].selected = val;
  },
};

export const getters = {
  getAllPrices(state) {
    return state.allShifts
      .flatMap((shift) => shift.formData)
      .map((data) => +data.price);
  },
  maxPrice(state, { getAllPrices }) {
    return getAllPrices.length ? Math.max(...getAllPrices) : 0;
  },
  minPrice(state, { getAllPrices }) {
    return getAllPrices.length ? Math.min(...getAllPrices) : 0;
  },
};
