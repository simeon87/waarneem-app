export const state = () => ({
  sidebarIsOpen: false,
  onEdit: false,
  maxDates: 10,
});

export const mutations = {
  openSidebar(state) {
    state.sidebarIsOpen = true;
  },
  exitSidebar(state) {
    state.sidebarIsOpen = false;
    state.onEdit = false;
  },
  edited(state) {
    state.onEdit = true;
  },
};
