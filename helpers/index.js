export function addZero(str) {
  return str.toString().padStart(2, '0');
}